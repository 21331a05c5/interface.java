interface Person{
   void displayMsg();
}
class Student implements Person{
   public void displayMsg() {
      System.out.println("Student class \n ");
   }
}
class Professor implements Person{
   public void displayMsg() {
      System.out.println("\nProfessor class");
   }
}
class Main{
   public static void main(String args[]) {
      Person p1 = new Student();
      p1.displayMsg();
      Person p2 = new Professor();
      p2.displayMsg();
   }
}
